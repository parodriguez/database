# Configuración Bases de Datos & VPN

## Configuracion del Servidor

Primero utilizamos `scp` para copiar de manera segura nuestras llaves públicas al servidor 

```shell
scp -i LlaveEquipo_pub becarios@database.becarios.tonejito.info:~
```

Y después, dentro del servidor las añadimos al archivo de llaves autorizadas en el directorio *.ssh/*

```shell
cat JesusPacheco_rsa.pub >> .ssh/authorized_keys 
cat JorgeFerrusca_rsa.pub >> .ssh/authorized_keys 
cat RubenDuran_rsa.pub.pub >> .ssh/authorized_keys 
```

Se crearon todos los usuarios con su respectiva contraseña y se añadieron al grupo *wheel*

```shell
usermod -aG wheel jorge-ferrusca
usermod -aG wheel jesus-pacheco
usermod -aG wheel ruben-duran
```

Posteriormente, modificando el archivo sudoers con `visudo` le dimos los permisos correspondientes a dicho grupo.

### Instalación de MySQL

Primero, descargamos el RPM correspondiente a la version de nuestro SO, en este caso, para CentOS7 el paquete es mysql80-community-release-el7-1.noarch.rpm.

Una vez descargado añadimos paquete a la lista de remositorios de nuestro sistema:

```shell
sudo rpm -Uvh mysql80-community-release-el7-1.noarch.rpm
```

Listamos las versiones disponibles disponibles:

```shell
yum repolist all | grep mysql
```

Y activamos el subrepositorio del release deseado (MySQL v5.7):

```shell
sudo yum-config-manager --enable mysql57-community
```

Y con esto, instalamos la version seleccionada en el repositorio:

```shell
sudo yum install mysql-community-server
```

Ahora, solo habilitamos (enable) y activamos el servicio:

```
sudo systemctl enable mysqld.server 
sudo systemctl start mysqld.server
```

### Instalación de PostgreSQL

Listamos los paquetes disponibles:

```shell
yum list postgresql*
```

Instalamos la versión deseada (En nuestro caso la 10):

```shell
sudo yum install postgresql10-server.x86_64
```

Inicializamos la base de datos en PGDATA:

```shell
sudo /usr/pgsql-10/bin/postgresql-10-setup initdb
```

Y con esto, podemos inicializar nuestro servicio:

```shell
sudo systemctl status postgresql-10
```


### Instalación de phpMyAdmin

Ahora. Para instalar phpMyAdmin mediante *yum*:

```shell
sudo yum -y install phpmyadmin
```

Instalamos también apache para poder hacer uso de la interfaz:

```shell
sudo yum -y install httpd
```

Y modificamos el archivo `/etc/httpd/conf.d/phpMyAdmin.conf`

En donde dentro del bloque de `Directory /usr/share/phpMyAdmin`, habilitaremos el acceso remoto de los siguientes equipos:

```apache
<Directory /usr/share/phpMyAdmin/>
   AddDefaultCharset UTF-8
   <IfModule mod_authz_core.c>
     # Apache 2.4
     <RequireAny>
      Require ip 127.0.0.1
      Require host tonejito.cf
      Require host becarios.tonejito.info
      Require host becarios.priv.tonejito.info
      Require host storage.becarios.tonejito.info
      Require host directory.becarios.tonejito.info
      Require host mail.becarios.tonejito.info
      Require host web.becarios.tonejito.info
      Require ip 10.0.8.0/24
      Require ip 132.247.0.0/16
      Require ip 132.248.0.0/16
      Require ip ::1
     </RequireAny>
   </IfModule>
</Directory>
```

Finalmente, iniciamos el servicio de apache:

```shell
sudo systemctl start httpd
```
    
### Configuración para Redmine

Para configurar el acceso a Redmine es necesario acceder a la consola de 
PostgreSQL para esto ejecutamos los comandos:

```shell
    sudo su postgres
```

Se abrira un bash, algo asi como bash-4.2$, aqui colocamos psql despues
del $ y ya estamos en la consola.

Ahora creamos un usuario y una base de datos para Redmine con los siguientes
comandos:

```sql
    CREATE ROLE redmine LOGIN ENCRYPTED PASSWORD 'your_password' NOINHERIT VALID UNTIL 'infinity';
    CREATE DATABASE redmine WITH ENCODING='UTF8' OWNER=redmine;
```

Una vez hecho esto solo resta configurar un par de archivos para permitirle 
a el servidor de Redmine conectarse a la base de datos.

#### Archivos:

*/var/lib/pgsql/10/data/postgresql.conf*

En este archivo todo lo que tenemos que hacer es configurar la siguiente linea
como se muestra

`listen_address = '*'`

*/var/lib/pgsql/10/data/pg_hba.conf*

En este archivo todo lo que tenemos que hacer es permitir la conexión desde 
el servidor donde se encuentra Redmine añadiendo las siguientes dos lineas hasta
el final del archivo

host    all         all         `<IP_Servidor>`/16    trust
host    all         all         10.8.0.0/24         trust

`<IP_Servidor>` es la IP Pública del servidor que tiene Redmine instalado y el otro 
Net ID pertenece a las IP's que asigna el servicio de VPN.

## MySQL

Para instalar MySQL como cliente ejecutamos el siguiente comando:

```shell
    sudo apt-get install mysql-server mysql-client
```

### Configuración de MySQL

Posteriormente, para poder acceder al manejador de base de datos, se recupera la contraseña que genera el SO a la hora de la instalación:

```shell
cat /var/log/mysqld.log | grep "temporary password"
```

Después, iniciaremos el cliente pero omitiendo la tabla de permisos con `skip-grant-tables` para así poder establecer un nuevo password. 

Se intentó hacer como lo vimos en clase, sin embargo tuvimos algun pequeño problema, por lo que otra forma de hacerlo fue modificando el archivo `/etc/my.cnf` añadiendo la directiva siguiente:

```shell
[mysqld]
skip-grant-tables
```

Con esto, iniciamos mysql de forma normal con el usuario *root*.

Ahora, creamos una nueva contraseña para dicho usuario:

```mysql
SET PASSWORD FOR root@'localhost' = PASSWORD('******');
```

Salimos y ahora sí, podremos iniciar sesión con la nueva contraseña de root en mysql.

Con esto, iniciamos sesión ya con la cuenta bien establecida, no sin antes eliminar la directiva que ejecutaba la opcion *skip-grant-tables*.

```shell
mysql -u root -p
```

Al ingresar, crearemos una base de datos llamada *wordpress* para el equipo de Web:

```mysql
CREATE DATABASE wordpress
```

Y añadiremos evidentemente algunos usuarios para que puedan acceder a dicha base desde el otro equipo.

La forma de añadirla será practicamente la misma, ejemplo: 

```mysql
CREATE USER 'web01'@'web.becarios.tonejito.info' IDENTIFIED BY '******';
```

Y le(s) daremos permisos para que manipulen la base que acabamos de crear:

```mysql
GRANT ALL ON wordpress.* TO 'web01'@'web.becarios.tonejito.info';
```


### Replicación de MySQL

Para poder hacer la replicacion debemos conectarnos con otro servidor que 
tambien tenga instalado el servicio de MySQL, una vez tengamos ambos servidores
uno va a fungir como Maestro que contendra las bases de datos y el Esclavo que 
se encargara de hacer una copia de las mismas.

En este caso se muestra la configuración del Servidor Maestro

Lo primero que hacemos es editar el archivo de configuracio de MySQL el cual 
encontramos en /etc/my.cnf

Abrimos el archivo para editarlo con el siguiente comando:

```shell
   sudo nano /etc/my.cnf 
```

Ya abierto el archivo estamos listos para editarlo, debajo de la seccion [mysqld]
colocamos las siguientes lineas:

## my.cnf

```
bind-address = X.X.X.X
server-id = 1
log_bin = /var/log/mysql/mysql-bin.log
binlog_do_db = wordpress
```

En la linea de bind-address colocamos la dirección IP pública de nuestro Servidor
Maestro, escojemos un ID para el Servidor el cual debe ser diferente del que se
le asigne a el Servidor Esclavo, con log_bin configuramos donde queremos que se 
guarde la bitácora y con binlog_do_db especificamos la base de datos que queremos 
replicar, podemos tener muchas de estas lineas indicando diversas bases de datos
en este caso solo vamos a replicar la basde de datos de wordpress.

Reiniciamos el servicio de MySQL ejecutando el comando:

```shell
    sudo systemctl restart mysqld
```

Una vez hecho esto debemos ingresar a la consola de MySQL para esto ejecutamos
el siguiente comando y e ingresamos la contraseña de MySQL.

```shell
    sudo mysql -u root -p
```

Creamos un usuario nuevo y le damos permisos de replicar y tambien le asignamos 
una contraseña.

```sql
    GRANT REPLICATION SLAVE ON *.* TO 'slave-storage'@'%' IDENTIFIED BY 'XXXXXX';
```

Aqui se esta creando un nuevo usuario llamado `slave-storage`, se le estan dando
permisos de replicacion y se le esta asignando la contraseña XXXXXX.

Luego ejecutamos:

```sql
    FLUSH PRIVILEGES;
```

Nos cambiamos de base de datos a la que queremos replicar con:

```sql
    USE wordpress;
```

Bloqueamos la base para protegerla de cualquier cambio con:

```sql
    FLUSH TABLES WITH READ LOCK;
```

Mostramos el estado de la base de datos actual con:

```sql
    SHOW MASTER STATUS;
```

Deberiamos ver algo como lo siguiente:

```
+------------------+----------+--------------+------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB |
+------------------+----------+--------------+------------------+
| mysql-bin.000001 |      107 | newdatabase  |                  |
+------------------+----------+--------------+------------------+
1 row in set (0.00 sec)
```

Esos parametros nos indican el estado actual de esta base de datos y 
resultaran bastante utiles mas adelante por lo que nos conviene tomar
nota de ellos.

En una nueva terminal sin cerrar la actual ejecutamos el comando:

```shell
    sudo mysqldump -u root -p --opt wordpress > wordpress.sql
```

Este comando nos permitira exportar la base de datos a un archivo .sql
que debemos proporcionar al Servidor Esclavo para que realice sus configuraciones
a traves de un metodo serguro.

En la primera consola de MySQL ejecutamos los comandos:

```sql
    UNLOCK TABLES;
    QUIT;
```

Para desbloquear las tablas anteriormente bloqueadas por proteccion y salimos 
de la consola de MySQL.

El Servidor Esclavo se encargara de realizar el resto de la configuracion.

## VPN

Ingresamos con nuestro usuario con privilegios de administrador y actualizamos
la lista de paquetes para asegurarnos de que tenemos las versiones mas 
actualizadas ejecutando el comando:

```shell
    sudo yum update -y
```

Posterioremente instalamos Enterprise Linux (EPEL), el cual es un repositorio
a cargo de el Proyecto Fedora en donde podemos encontrar paquetes populares
que no son estandar, en nuestro caso OpenVPN no se encuentra en los paquetes
por defecto de CentOS, de ahí la necesidad de instalar EPEL.

```shell
    sudo yum install epel-release -y
```

Volvemos a actualizar la lista de paquetes en busca de alguna actualizacion de
EPEL.

```shell
    sudo yum update -y
```

Ahora estamos listos para instalar el paquete de openvpn, ademas instalaremos
wget para poder descargar un archivo comprimido que contiene una serie de 
paquetes que nos van a permitir crear una entidad certificadora de manera
sencilla, ejecutamos el siguiente comando:

```shell
    sudo yum install -y openvpn wget
```

`Nota: Por motivos de serguridad es altamente recomendable hacer todo lo
referente a Easy Rsa en un equpo que no tenga nada que ver con el servidor`

Descargamos el archivo comprimido con los paquetes de Easy RSA, para crear
nuestra entidad certificadora.

Se recomienda descargar la versión dos por que existe mas documentación para
esta versión.

```shell
    wget -O /tmp/easyrsa https://github.com/OpenVPN/easy-rsa-old/archive/2.3.3.tar.gz
```

Extraemos los archivos en el directorio de archivos temporales

```shell
    sudo tar xfz /tmp/easyrsa
```

El comando anterior creara un nuevo directorio llamado easy-rsa-old-2.3.3,lo
que haremos a continuación sera copiar todos los paquetes que nos permiten 
montar nuestra entidad certificadora, para esto ejecutamos los siguientes
comandos:

```shell
    sudo mkdir /etc/easy-rsa
    sudo cp -rf /tmp/easy-rsa-old-2.3.3/easy-rsa/2.0/* /etc/easy-rsa
```

Para finalizar la preparacion de los archivos de la entidad certificadora 
cambiamos el dueño de la carpeta que acabamos de crear con el comando:

```shell
    sudo chown <nombre-usuario> /etc/easy-rsa
```

Cambiar `<nombre-usuario>` por el nombre del usuario que creaste y al cual
le proporcionaste privilegios de administrador.

### Configuración de OpenVPN - Server

Lo primero que tenemos que hacer es copiar una de las plantillas de configuración
que vienen incluidas cuando descargamos openvpn para esto ejecutamos:

```shell
    sudo cp /usr/share/doc/openvpn-2.4.4/sample/sample-config-files/server.conf /etc/openvpn
```

El copiar el archivo de configuración a este lugar no es coincidencia, openvpn
toma los archivos de configuración precisamente de esa ruta "/etc/openvpn", ya
en su lugar el archivo de configuración estamos listos para configurar.

A continuacion se muestra el contenido del archivo de configuracion:

### **server.conf**
```
port 1194
proto udp
dev tun
ca server/ca.crt
cert server/server.crt
key server/server.key  # This file should be kept secret
dh server/dh2048.pem
topology subnet
server 10.8.0.0 255.255.255.0
ifconfig-pool-persist ipp.txt
client-to-client                             
keepalive 10 120                             
cipher AES-256-CBC                           
persist-key                                  
persist-tun                                  
status server/openvpn-status.log             
verb 3                                       
explicit-exit-notify 1
```

### Generación de Llaves y Certificados

Una vez realizada la parte de la confuguracion del servidor ahora vamos a trabajar
con la generación de certificados para esto debemos acceder a la maquina donde
instalamos Easy-RSA y crear un nuevo directorio donde vamos a almacenar las 
llaves a generar, ejecutamos el comando:

```shell
    sudo mkdir /etc/openvpn/easy-rsa/keys
```

Posteriormente tenemos que editar el archivo vars para establecer ciertos 
parametros por defecto a la hora de crear los certificados y facilitar la 
emision de los mismos, para esto ejecutamos el siguiete comando:

```shell
    sudo nano /etc/openvpn/easy-rsa/vars
```

Los parametros a modificar dentro de este archivo deberan verse algo como lo
siguiente:

### **server.conf**
```
export KEY_COUNTRY="MX"
export KEY_PROVINCE="Coyoacan"
export KEY_CITY="CDMX"
export KEY_ORG="UNAM"
export KEY_EMAIL="jpacheco@mail.com"
export KEY_EMAIL=jpacheco@mail.com
export KEY_CN=database.becarios.tonejito.info
export KEY_NAME=server
export KEY_OU=CERT
```

Se recomienda que cada uno de estos campos se llene con la información 
corresponditente, siendo el mas importante de ellos el `KEY_CN` aqui debemos 
colocar el nombre de dominio que el DNS traduce en la direccion IP del servidor
donde esta alojado el servicio de VPN, otro campo importante es el `KEY_NAME`, que
es el nombre que tendran la llave y el certificado generados cuando creemos la
entidad certificadora y como por defecto en el archivo de configuracion server.conf
se especifica el nombre server lo dejamos como server. Los demas campos los 
llenamos de acuerdo a nuestra propia información.


Una vez hecho lo anterior necesitamos recargar la configuración del archivo 
vars, para esto ejecutamos:

```shell
    cd /etc/openvpn/easy-rsa
    source ./vars
```

Ahora estamos listos para crear nuestra Autoridad Certificadora, primero ejecutamos 
el comando:

```shell
    ./clean-all
```

Para eliminar cualquier llave o archivo que pudieramos haber creado en el pasado
es decir,  borramos todo para iniciar desde 0.

Luego creamos la Autoridad Certificadora con:

```shell
    ./build-ca
```

Este comando generara un archivo llamado `ca.key` el cual es un archivo muy 
sensible que no deberia estar al alcance de cualquiera ya que es la llave privada
de la CA y cualquiera con acceso a ella podria firmar certificados para que 
puedan acceder a nuestra VPN por eso es impotante protegerla muy bien.

Ya con la llave en su lugar generamos un certificado y una llave para nuestro
propio servidor VPN con el comando:

```shell
    ./build-key-server server
```

Observaremos que se preguntaran varios parametros, sin embargo estos son los que
hemos definido anteriormente en el archivo vars por lo que bastara con ir pulsando
enter para que se tomen los valores por defecto previamente configurados, incluso
se nos pide una contraseña para añadir una capa mas de seguridad, de desearlo se 
escoje una, en este caso por simplicidad se pulsa enter para que no se asigne 
ninguna contraseña.

Ahora solo resta crear la llave Diffie-Hellman para intercambio de archivos, para
esto ejecutamos el comando:

```shell
  ./build-dh  
```

Cabe destacar que esto `podría tardar varios minutos en ejecutarse`.

Finalmente copiamos esta clave de intercambio y las llaves y certificados 
generados del directorio /etc/easy-rsa/keys en la maquina aislada a 
/etc/openvpn/<carpeta> en el servidor que tiene el servicio de openvpn con los 
comandos:

```shell
   sudo cd /etc/openvpn/easy-rsa/keys
   sudo cp sh2048.pem ca.crt server.crt server.key /etc/openvpn *1
```

`<carpeta>` sustituir esta marca por el nombre de la carpeda donde se indicó
se iban a guardar estos archivos que estamos moviendo, el nombre de la carpeta
que debemos poner lo podemos observar en el archivo de configuracion del servidor
*server.conf*, para este caso esa carpeta se llama server.

*1 Este comando esta hecho considerando que la CA se montó en el mismo server
que donde esta instalado openvpn, buscar la manera de copiar esos archivos desde
la maquina donde se definio la CA y pegarlos en la ruta indicada en el servidor.

Para no tener problemas con openssl copiamos la configuracion del mismo a la
misma ruta de los archivos anteriores con el comando:

```shell
    cp /etc/openvpn/easy-rsa/openssl-1.0.0.cnf /etc/openvpn/easy-rsa/openssl.cnf
```

### Configurando el reenvio de información

Para que la comunicación entre clientes se realice de manera correcta tenemos
que habilitar el reenvio IP de tal manera que cuando se reincie el servidor
esta configuracion se cargue de nuevo, para esto ejecutamos los sigueites comandos:

```shell
    sudo nano /etc/sysctl.conf
```

Una vez que nano abre el archivo escribimos la siguiente linea:

net.ipv4.ip_forward = 1

Guardamos cambios y cerramos el archivo y posteriormente reiniciamos el
servicio de red para que se aplique el cambio con :

```shell
    sudo systemctl restart network.service
```

### Iniciando el Servicio OpenVPN

Primero ejecutaos el siguiente comando para configurar que el servicio de 
openvpn se inicie en el arranque.

```shell
   sudo systemctl -f enable openvpn@server.service 
```

Luego iniciamos el servicio:

```shell
   sudo systemctl start openvpn@server.service 
```

Finalmente verificamos que el servicio este corriendo con:

```shell
   sudo systemctl status openvpn@server.service
```

### Configuración para el cliente

En nuestro equipo aislado donde fue configurada la Autoridad Certificadora ejecutamos:

```shell
   cd /etc/easy-rsa 
```

Ahi es el sitio donde tenemos todos los progras que nos permiten usar la CA facilmente
para este caso necesitamos crear y firmar un certificado para un cliente, para esto 
ejecutamos el siguiente comando:

```shell
   ./build-key <nombre-cliente> 
```

Despues de ejecutar este comando se nos volveran a preguntar los parametros que
habiamos configurado en el archivo vars, dejamos todo por defecto menos el KEY_CN
ahi tenemos que colocar el nombre de dominio del servidor para el cual estamos 
emitiendo el certificado en este caso podriamos pones mail.becarios.tonejito.info
por ejemplo.

`<nombre-cliente>` sustituir este parametro por un nombre que nos permita identificar
para quien fue emitido el certificado facilmente, por ejemplo mail.

El comando build-key nos generara dos archivos, el `<nombre-cliente>`.crt y `<nombre-cliente>`.key
estos archivos junto con el ca.crt los utilizaremos posteriormente para generar el archivo de 
configuracion con extension .ovpn que contendra toda la configuracion necesaria y que solo 
debera ser ejecutada en el cliente para poder conectarlo a la VPN.

La creacion del archivo ovpn asi como la ejecucion del mismo queda a cargo de 
cada uno de los clientes que deseen conectarse a la VPN, se les propocionara el
certificado de la CA así como su llave y su certificado.

## IP Tables

Para implementar iptables, creamos un script con las reglas que creímos convenientes:
(Consideramos más viable una politica de tipo DROP y aceptar manualmente tanto protocolos como equipos)

```shell
#!/bin/bash
#cleaning
sudo iptables -t filter -F 
sudo iptables -t filter -X
#allow dns
sudo iptables -A OUTPUT -p udp --dport 53 -j ACCEPT
# mantener conexiones establecidas
sudo iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT 
sudo iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT 
# allow loopback
sudo iptables -t filter -A INPUT -i lo -j ACCEPT 
sudo iptables -t filter -A OUTPUT -o lo -j ACCEPT 
#allow protocols 
sudo iptables -t filter -A INPUT -p tcp --dport 22 -j ACCEPT
sudo iptables -t filter -A OUTPUT -p tcp --dport 22 -j ACCEPT
sudo iptables -t filter -A OUTPUT -p tcp --dport 80 -j ACCEPT
sudo iptables -t filter -A INPUT -p tcp --dport 80 -j ACCEPT
sudo iptables -t filter -A OUTPUT -p tcp --dport 443 -j ACCEPT
sudo iptables -t filter -A INPUT -p tcp --dport 443 -j ACCEPT
sudo iptables -t filter -A OUTPUT -p tcp --dport 3306 -j ACCEPT
sudo iptables -t filter -A INPUT -p tcp --dport 3306 -j ACCEPT
sudo iptables -t filter -A OUTPUT -p tcp --dport 5432 -j ACCEPT
sudo iptables -t filter -A INPUT -p tcp --dport 5432 -j ACCEPT
#allow all from hosts
sudo iptables -A OUTPUT -d directory.becarios.tonejito.info -j ACCEPT
sudo iptables -A OUTPUT -d mail.becarios.tonejito.info -j ACCEPT
sudo iptables -A OUTPUT -d web.becarios.tonejito.info -j ACCEPT
sudo iptables -A OUTPUT -d storage.becarios.tonejito.info -j ACCEPT
sudo iptables -A INPUT -s directory.becarios.tonejito.info -j ACCEPT
sudo iptables -A INPUT -s mail.becarios.tonejito.info -j ACCEPT
sudo iptables -A INPUT -s web.becarios.tonejito.info -j ACCEPT
sudo iptables -A INPUT -s storage.becarios.tonejito.info -j ACCEPT
#tonejos
sudo iptables -A INPUT -s tonejito.cf -j ACCEPT 
sudo iptables -A INPUT -s becarios.tonejito.info -j ACCEPT
sudo iptables -A INPUT -s becarios.priv.tonejito.info -j ACCEPT
sudo iptables -A INPUT -s 10.0.8.0/24 -j ACCEPT
sudo iptables -A INPUT -s 132.247.0.0/16 -j ACCEPT
sudo iptables -A INPUT -s 132.248.0.0/16 -j ACCEPT
#sudo sudo iptables -A INPUT -p icmp -j ACCEPT
#sudo sudo iptables -A OUTPUT -p icmp -j ACCEPT
#allow pings
sudo iptables -A OUTPUT -p icmp -d directory.becarios.tonejito.info -j ACCEPT
sudo iptables -A OUTPUT -p icmp -d mail.becarios.tonejito.info -j ACCEPT
sudo iptables -A OUTPUT -p icmp -d web.becarios.tonejito.info -j ACCEPT
sudo iptables -A OUTPUT -p icmp -d storage.becarios.tonejito.info -j ACCEPT
sudo iptables -A INPUT -p icmp -s directory.becarios.tonejito.info -j ACCEPT
sudo iptables -A INPUT -p icmp -s mail.becarios.tonejito.info -j ACCEPT
sudo iptables -A INPUT -p icmp -s web.becarios.tonejito.info -j ACCEPT
sudo iptables -A INPUT -p icmp -s storage.becarios.tonejito.info -j ACCEPT
#Drop anything else >:c
sudo iptables -t filter -P INPUT DROP
sudo iptables -t filter -P FORWARD DROP
sudo iptables -t filter -P OUTPUT DROP
```

Una vez aplicadas las reglas, descargamos el servicio de iptables:

```shell
sudo yum install iptables-services
```

Activamos el servicio al reiniciar:

```shell
sudo systemctl enable iptables
```

Iniciamos el servicio:

```shell
sudo systemctl start iptables
```

Y hacemos persistentes las reglas actuales aplicadas:

```shell
sudo service iptables save
```
	
Cabe señalar que las iptables aplicadas, serán guardadas en:

`/etc/sysconfig/iptables`


## Referencias

1. PostgreSQL

- https://www.redmine.org/projects/redmine/wiki/HowTo_Install_Redmine_on_Debian_9?fbclid=IwAR2OnzGe5T9FZPlrjVDRw2imNyMhin_CxmKHLe9J9M24ukSuh2kZEEJQzQI

2. MySQL
    
- https://www.digitalocean.com/community/tutorials/how-to-set-up-master-slave-replication-in-mysql

3. OpenVPN

- https://www.digitalocean.com/community/tutorials/how-to-set-up-and-configure-an-openvpn-server-on-centos-7
- https://www.youtube.com/watch?v=XcsQdtsCS1U4

4. IP Tables

- http://linux-training.be/networking/ch14.html